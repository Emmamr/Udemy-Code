/*
var name = 'John';
console.log(name);

var lastName = 'Smith';
console.log(lastName);

var age = 26;
console.log('age');

var fullAge = true;
console.log(fullAge);
*/

// Lecture: Variables 2

/* var name = 'John';
var age = 26;

//console.log(name + age);
//console.log(age + age);

var job, isMarried;

//console.log(job);

job = 'Teacher';
isMarried = false;

console.log(name + ' is a ' + age + ' years old ' + job + '. Is he married? ' + isMarried + '.');

age = 'thirty six';
job = 'driver';

console.log(name + ' is a ' + age + ' years old ' + job + '. Is he married? ' + isMarried + '.');
*/



//----------------------------------------------------------------------------------------------
//Lecture: Operators
/*
var now = 2018;
var birthyear = now - 22;

birthyear = now - 22 * 2;

console.log(birthyear);

var ageJohn = 30;
var ageMark = 30;

ageJohn = (3+5) * 4 - 6;

console.log(ageJohn);
*/

//----------------------------------------------------------------------------------------------
//Lecture: if/else statements

/*

var name = 'John';
var age = 26;
var isMarried = 'no';

if(isMarried === 'yes'){
    console.log(name + ' is married')
} else {
    console.log(name + ' will hopefully marry soon! :)')
}

isMarried = false;

if(isMarried) {
    console.log('Yes!');
} else{
    console.log('NO!');
}

*/

//----------------------------------------------------------------------------------------------
//Lecture: Boolean and Switch Statements

/* 

Basic Boolean Logic: NOT, AND & OR

-AND ( && ) => True if ALL are True
-OR  ( || ) => True if of ONE is TRUE
-NOT ( ! )  => inverts true/false value

*/

 var age = 15; 

if (age < 20 ){
    //console.log('John is a teenager');
} else if (age >= 20 && age < 30 ){
    //console.log('John is a young man.');
}else {
    //console.log('John is a man.');
}

var job = 'teacher';

//job = prompt('what does john do?');

switch(job){
    case 'teacher':
        //console.log('John teaches kids.');
        break;
    case 'driver':
        //console.log('John drives a cab in Lisbon.');
        break;
    case 'cop':
        //console.log('John helps fight crime.');
        break;
    default:
        //console.log('John does something else.');
}



// CODING CHANLLANGE

var heightFriendOne,
    heightFriendTwo,
    ageFriendThree,
    ageFriendOne,
    ageFriendTwo,
    ageFriendThree;

heightFriendOne = 180;

heightFriendTwo = 154;

heightFriendThree = 170;

ageFriendOne = 16;

ageFriendTwo = 15; 

ageFriendThree = 14;

one = heightFriendOne + (5 * ageFriendOne);

two = heightFriendTwo + (5 * ageFriendTwo);

three = heightFriendThree + (5 * ageFriendThree);

if (one > two &&  one > three){
    //console.log('Friend One wins with a score of: ' + one);
} else if ( one < two && two > three){
    //console.log('Friend Two wins with a score of: ' + two);
} else if ( one < three && three > two ){
    //console.log('Frien Three wins with a score of: ' + three);
} else {
    //console.log('IT\'s a tie!');
} 


//----------------------------------------------------------------------------------------------

// Lecture: FUNCTIONS 

function calculateAge (yearOfBirth){
    var age = 2018 - yearOfBirth;
    return age;
}

var ageJohn = calculateAge(1995);
var ageMike = calculateAge(1969);
var ageMary = calculateAge(1948);

/* console.log(ageJohn);
console.log(ageMike);
console.log(ageMary); */


function yearsUntilRetirement(name, year){
    var age = calculateAge(year);
    var retirement = 65 - age;
    
    if (retirement >= 0){
        //console.log(name + 'retires in ' + retirement + ' years.');
    } else{
        //console.log( name + ' has already retired.');
    }
    
}

yearsUntilRetirement('John', 1995);
yearsUntilRetirement('Mike', 1969);
yearsUntilRetirement('Mary', 1948);

//----------------------------------------------------------------------------------------------

// Lecture: Statements and Expressions 

//This is a Statement 
function someFun(par){
    //code 
}

//This is an Expression
var someFun = function(par){
    //code
}

//Expressions
3 + 4;
var x = 3;

//Statements 
if (x === 5) {
    //do something
}



//----------------------------------------------------------------------------------------------

// Lecture: Arrays

var names = [ 'John', 'Jane', 'Mark'];
var years = new Array (1990, 1969, 1948);

//console.log(names[2]);
names[1] = 'Ben';
//console.log(names);

var john = ['John', 'Smith', 1990, 'designer', false];

john.push('blue'); // Adds to the end item of the array
john.unshift('Mr.'); // Add to the beginning of the array
john.pop(); //Revomes the last items of the array
john.shift(); // Removes the first item of the array

//console.log(john);

if (john.indexOf('teacher') === -1 ) {
    //console.log('John is Not a Teacher.')
}



//----------------------------------------------------------------------------------------------

// Lecture: Objects

var john = {
    name: 'John',
    lastName: 'Smith',
    yearOfBirth: 1990,
    job: 'teacher',
    isMarried: false
};
// console.log(john.lastName);
// console.log(john['lastName']);

var xyz = 'job';
// console.log(john[xyz]);

john.lastName = 'Miller';
john['job'] = 'programmer';

// console.log(john.lastName);
// console.log(john.job);

// console.log(john);

var jane = new Object();
jane.name = 'Jane';
jane.lastName = 'Smith';
jane['yearOfBirth'] = 1969;
jane['job'] = 'retired';
jane['isMarried'] = true;

// console.log(jane);


//----------------------------------------------------------------------------------------------

// Lecture: Objects and methods

//v1.0
/*
var john = {
    name: 'John',
    lastName: 'Smith',
    yearOfBirth: 1990,
    job: 'teacher',
    isMarried: false,
    family: ['Jane', 'Mark', 'Bob'],
    calculateAge: function(){
        return 2018 - this.yearOfBirth;
    }
};


console.log(john.calculateAge());

var age = john.calculateAge();

john.age = age;

console.log(john);
*/

// v2.0

var john = {
    name: 'John',
    lastName: 'Smith',
    yearOfBirth: 1990,
    job: 'teacher',
    isMarried: false,
    family: ['Jane', 'Mark', 'Bob'],
    calculateAge: function(){
        this.age = 2018 - this.yearOfBirth;
    }
};

john.calculateAge();
console.log(john);


var mike = {
    yearOfBirth: 1950,
    calculateAge: function(){
        this.age = 2018 - this.yearOfBirth;
    }
};

mike.calculateAge();
//console.log(mike);


var emmanuel = {
    firstName: 'Emmanuel ',
    lastName: 'Mosqueda Ramirez ',
    dayOfBirth: 7,
    monthOfBirth: 12,
    yearOfBirth: 1995,
    job: 'Programmer',
    calculateAge: function(){
        this.age = 2018 - this.yearOfBirth;
    }
};

emmanuel.calculateAge();
console.log('Hello my name is ' + emmanuel.firstName + emmanuel.lastName 
+ 'I\'m a ' + emmanuel.age + ' years old and my birthday is on ' 
+ emmanuel.monthOfBirth + '/' + emmanuel.dayOfBirth + '/' 
+ emmanuel.yearOfBirth + '.' + ' My job is being a ' + emmanuel.job );



//----------------------------------------------------------------------------------------------

// Lecture: Loops

for ( var i = 0; i <= 10; i++){
    //console.log(i);
}

var namesLoop = ['John', 'Jane', 'Mary', 'Mark', 'Bob'];

//For Loops
for ( var i = 0; i < namesLoop.length; i++ ){
    //console.log(namesLoop[i]);
}


for ( var i = namesLoop.length - 1; i >= 0; i-- ){
    //console.log(namesLoop[i]);
}

//While Loops
var i = 0;
while(i < namesLoop.length){
    //console.log(namesLoop[i]);
    i++;
};

for (var i = 1; i <= 5; i++){
    //console.log(i);

    if( i === 3){
        break;
    }
}

for (var i = 1; i <= 5; i++){
    if( i === 3){
        continue;
    }
    //console.log(i);
}

//CODING CHALLENGE 2

var yearsChallenge = [1963, 1963, 1983, 1985, 1993, 1995, 2007, 2011];



function printFullAge(yearsChallenge){
    var emptyArray = [];
    var fullAges = [];

    for (var i = 0; i < yearsChallenge.length; i++){
        emptyArray.push(2018 - yearsChallenge[i]);
    }
    
    for ( var i = 0; i < emptyArray.length; i++){
    
        if ( emptyArray[i] >= 18){
            console.log('Person ' + (i + 1) + ' is ' + emptyArray[i] + ' years old, and is full age');
            fullAges.push(true);
        } else {
            console.log('Person ' + (i + 1) + ' is ' + emptyArray[i] + ' years old, and is NOT full age');
            fullAges.push(false);
        }
    }
    return fullAges
}

var full_1 = printFullAge(yearsChallenge);
var full_2 = printFullAge([2012, 1915, 1999]);
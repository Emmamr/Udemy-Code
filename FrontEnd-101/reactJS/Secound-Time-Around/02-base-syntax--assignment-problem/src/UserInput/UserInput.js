import React from 'react';

const UserInput = (props) => {

    const style = {
        display: 'block',
        border: '2px solid red',
        margin: 'auto'
    };

    return <input 
    style={style}
    type="text" 
    onChange={props.changed} 
    value={props.currentName}/>
}

export default UserInput;